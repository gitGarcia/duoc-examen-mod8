
package Sql;

import Conexion.Conectar;
import Modelo.Boleta;
import Modelo.Producto;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;


public class Consultas {
    
    public static int numBoleta()
    {
        ResultSet rs = null;
        int r =0;
        try{
            Connection con = Conectar.getConecta();
            String query = "select max(nro_boleta) from boleta";
            PreparedStatement ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            while(rs.next()){
                r= rs.getInt(1);
            }
            con.close();
        }catch(SQLException e){
        }
        return r;
    }
    
    public static boolean agregar(Boleta b, Producto p)
    {
        try{
            Connection con = Conectar.getConecta();
            String query = "{call PA_NUEVA_BOLETA(?,?,?,?,?,?)}";
            CallableStatement cs= con.prepareCall(query);
            cs.setInt(1, b.getMontoBol());
            cs.setInt(2, b.getVendedorId());
            cs.setInt(3, p.getCodPro());
            cs.setInt(4, p.getQty());
            cs.setInt(5, p.getUnitario());
            cs.setInt(6, p.vtotal());
            cs.execute();
            cs.close();
            con.close();
            JOptionPane.showMessageDialog(null, "Agregado con éxito");
            return true;
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Error al agregar "+e.getLocalizedMessage());
            return false;
        }
    }
    
    
    public static int verificarPro(int cod)
    {
        int a;
        try{
            Connection con = Conectar.getConecta();
            String query = "select * from stock where cod_producto=?";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1, cod);
            a= ps.executeUpdate();
            ps.close();
            con.close();
        }catch(SQLException e){
            a=-1;
        }
        return a;
    }
    
    public static int stockPro(int cod)
    {
        int a=0;
        try{
            Connection con = Conectar.getConecta();
            String query = "select stock_actual from stock where cod_producto=?";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1, cod);
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                a=rs.getInt("STOCK_ACTUAL");
                System.out.println(rs.getString("STOCK_ACTUAL"));
            }
            ps.close();
            con.close();
        }catch(SQLException e){
            a=-1;
        }
        return a;
    }
    
    public static ResultSet vendedor()
    {
        ResultSet rs =null;
        try{
            Connection con = Conectar.getConecta();
            String query = "select pnombre || ' ' || appaterno from vendedor order by id_vendedor";
            PreparedStatement ps = con.prepareStatement(query);
            rs = ps.executeQuery();
        }catch(SQLException e){
            
        }
        return rs;
    }
    
    public static int qty(int cod)
    {
        int a=-1;
        try{
            Connection con = Conectar.getConecta();
            String query = "select stock_actual from stock where cod_producto=?";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1, cod);
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                a = rs.getInt(1);
            }
        }catch(SQLException e){
            
        }
        return a;
    }
    
    public static ResultSet listarStock()
    {
        ResultSet rs= null;
        try{
            Connection con = Conectar.getConecta();
            String query = "BEGIN PA_GET_STOCK(?); END;";
            CallableStatement cs= con.prepareCall(query);
            cs.registerOutParameter(1, OracleTypes.CURSOR);
            cs.execute();
            rs = ((OracleCallableStatement)cs).getCursor(1);
        }catch(SQLException e){
            System.out.println("Error la consulta "+e.getLocalizedMessage());
        }
        return rs;
    }
    
    public static ResultSet listarDetalle()
    {
        ResultSet rs= null;
        try{
            Connection con = Conectar.getConecta();
            String query = "BEGIN PA_GET_DETALLE(?); END;";
            CallableStatement cs= con.prepareCall(query);
            cs.registerOutParameter(1, OracleTypes.CURSOR);
            cs.execute();
            rs = ((OracleCallableStatement)cs).getCursor(1);
        }catch(SQLException e){
            System.out.println("Error la consulta "+e.getLocalizedMessage());
        }
        return rs;
    }
    
    public static ResultSet buscarStock(int cod)
    {
        ResultSet rs= null;
        try{
            Connection con = Conectar.getConecta();
            String query = "BEGIN PA_BUSCAR_STOCK(?,?); END;";
            CallableStatement cs= con.prepareCall(query);
            cs.setInt(1, cod);
            cs.registerOutParameter(2, OracleTypes.CURSOR);
            cs.execute();
            rs = ((OracleCallableStatement)cs).getCursor(2);
        }catch(SQLException e){
            System.out.println("Error la consulta "+e.getLocalizedMessage());
        }
        return rs;
    }
    
    public static ResultSet buscarBoleta(int cod)
    {
        ResultSet rs= null;
        try{
            Connection con = Conectar.getConecta();
            String query = "BEGIN PA_BUSCAR_BOLETA(?,?); END;";
            CallableStatement cs= con.prepareCall(query);
            cs.setInt(1, cod);
            cs.registerOutParameter(2, OracleTypes.CURSOR);
            cs.execute();
            rs = ((OracleCallableStatement)cs).getCursor(2);
        }catch(SQLException e){
            System.out.println("Error la consulta "+e.getLocalizedMessage());
        }
        return rs;
    }
    
    public static int eliminarBol(int cod)
    {
        int a;
        try{
            Connection con = Conectar.getConecta();
            String query = "delete from detalle_boleta where nro_boleta=?";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1, cod);
            a= ps.executeUpdate();
            query = "delete from detalle_boleta where nro_boleta=?";
            ps = con.prepareStatement(query);
            ps.setInt(1, cod);
            a= a + ps.executeUpdate();
            ps.close();
            con.close();
            JOptionPane.showMessageDialog(null, "Eliminado con Éxito");
        }catch(SQLException e){
            a=-1;
            JOptionPane.showMessageDialog(null, "Error al eliminar");
        }
        return a;
    }
    
    public static int modificarBol(Boleta b, Producto p)
    {
        int a;
        try{
            Connection con = Conectar.getConecta();
            String query = "update boleta set monto_boleta=?, id_vendedor=? where nro_boleta = ?";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1, p.vtotal());
            ps.setInt(2, b.getVendedorId());
            ps.setInt(3, b.getNumBol());
            a= ps.executeUpdate();
            query = "update detalle_boleta set cantidad=?, valor_unitario=?, valor_total=? where nro_boleta = ? and cod_producto=?";
            ps = con.prepareStatement(query);
            ps.setInt(1, p.getQty());
            ps.setInt(2, p.getUnitario());
            ps.setInt(3, p.vtotal());
            ps.setInt(4, b.getNumBol());
            ps.setInt(5, p.getCodPro());
            a=a+ ps.executeUpdate();
            ps.close();
            con.close();
            JOptionPane.showMessageDialog(null, "Boleta Modificada con éxito");
        }catch(SQLException e){
            a=-1;
            JOptionPane.showMessageDialog(null, "Error al modificar boleta");
        }
        return a;
    }
    
    public static int modificarStock(Producto p)
    {
        int a;
        try{
            Connection con = Conectar.getConecta();
            String query = "update stock set stock_minimo=?, stock_actual=? where cod_producto=?";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1, p.getMinimo());
            ps.setInt(2, p.getQty());
            ps.setInt(3, p.getCodPro());
            a= ps.executeUpdate();
            ps.close();
            con.close();
            JOptionPane.showMessageDialog(null, "Stock Modificado con éxito");
        }catch(SQLException e){
            a=-1;
            JOptionPane.showMessageDialog(null, "Error al modificar stock");
        }
        return a;
    }
}

