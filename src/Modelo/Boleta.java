
package Modelo;

import java.util.ArrayList;
import java.util.Date;


public class Boleta {
    private int numBol, vendedorId, montoBol;
    private Date fechaBol;
    
    public Boleta() {
    }

    public int getNumBol() {
        return numBol;
    }

    public void setNumBol(int numBol) {
        this.numBol = numBol;
    }

    public int getVendedorId() {
        return vendedorId;
    }

    public void setVendedorId(int vendedorId) {
        this.vendedorId = vendedorId;
    }

    public Date getFechaBol() {
        return fechaBol;
    }

    public void setFechaBol(Date fechaBol) {
        this.fechaBol = fechaBol;
    }

    public int getMontoBol() {
        return montoBol;
    }

    public void setMontoBol(int montoBol) {
        this.montoBol = montoBol;
    }
  
    
}
