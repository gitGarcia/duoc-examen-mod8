
package Modelo;


public class Producto {
    private int codPro, qty, minimo, unitario;

    public Producto() {
    }

    public int getCodPro() {
        return codPro;
    }

    public void setCodPro(int codPro) {
        this.codPro = codPro;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getUnitario() {
        return unitario;
    }

    public void setUnitario(int unitario) {
        this.unitario = unitario;
    }

    public int vtotal()
    {
        return unitario * qty;
    }

    public int getMinimo() {
        return minimo;
    }

    public void setMinimo(int minimo) {
        this.minimo = minimo;
    }
    
    
    
}
