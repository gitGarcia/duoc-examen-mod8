package Conexion;
import java.awt.HeadlessException;
import java.sql.*;
import javax.swing.JOptionPane;

/**
 *
 * @author linf
 */
public class Conectar {

    public static Connection getConecta() {

        Connection con = null;
        Statement st = null;

        try {

            Class.forName("oracle.jdbc.OracleDriver");
            String url = "jdbc:oracle:thin:@localhost:1521:XE";
            con = DriverManager.getConnection(url, "super_chinese", "1234");
        } catch (HeadlessException | ClassNotFoundException | SQLException e) {

            JOptionPane.showMessageDialog(null, "Conexion sin realizar " + e.getMessage());

        }
        return (con);
    }

    
}